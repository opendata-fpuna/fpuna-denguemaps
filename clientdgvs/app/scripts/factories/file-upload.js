'use strict';



angular.module('denguemapsApp')
  .factory('fileUpload', ['$http', '$q', function ($http, $q, toastr) {
    return {
      uploadFileToUrl: uploadFileToUrl
    }

    function uploadFileToUrl(file, uploadUrl){
      var defered = $q.defer();
      var promise = defered.promise;
      var fd = new FormData();
      fd.append('file', file);
      $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
          //console.log(data);
          defered.resolve(data);
        })
        .error(function(err){
          //toastr.error('La estructura de columnas del archivo no es válida','Error');
          defered.reject(err);
        });
      return promise;
    }

}]);
