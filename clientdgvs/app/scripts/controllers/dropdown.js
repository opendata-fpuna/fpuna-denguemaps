'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:AyudaCtrl
 * @description
 * # AyudaCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('DropdownCtrl', function ($scope, $log) {
    $scope.years = ['2009','2010','2011','2012','2013', '2014', '2015'];
    $scope.types = ['JSON','CSV'];

    $scope.status = {
      isopen: false
    };

    $scope.toggled = function(open) {
      $log.log('Dropdown is now: ', open);
    };

    $scope.toggleDropdown = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.status.isopen = !$scope.status.isopen;
    };
});