package com.app.utils.tables.dao;

import com.app.utils.tables.filter.BaseFilter;

import java.util.List;

/**
 * Created by rparra on 23/3/15.
 */
public interface GenericDao<T> {
    List<T> getEntities(List<String> names, List<List<BaseFilter<?>>> filters, Integer pageSize, Integer offset);
    Integer getEntitiesCount();
    Integer getFilteredEntitiesCount(List<String> names, List<List<BaseFilter<?>>> filters);
    String getAllFilteredEntities(List<List<BaseFilter<?>>> filters);
}
