package com.app.utils.tables.filter;

/**
 * Created by rparra on 23/3/15.
 */

import java.util.logging.Logger;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Define un filtro aplicado sobre un atributo del tipo String heredando los
 * atributos base de {@link utils.filter.BaseFilter}
 */
public class StringFilter extends BaseFilter<String> {
    /**
     * Agrega la condicion de like
     */
    private String like;

    /**
     * Agrega condicion de notLike
     */
    private String notLike;
    protected static Logger log = Logger.getLogger(StringFilter.class.getName());


    private StringFilter() {
        super();
        this.like = null;
        this.notLike = null;
    }

    public StringFilter(Class<?> clazz, String path) {
        super(clazz, path);
        this.like = null;
        this.notLike = null;
    }

    public StringFilter(Class<?> clazz, String... args) {
        super(clazz, args);
        this.like = null;
        this.notLike = null;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getNotLike() {
        return notLike;
    }

    public void setNotLike(String notLike) {
        this.notLike = notLike;
    }

    @Override
    public String getCondition() {
        List<String> conditions = new ArrayList<>();
        Boolean needsCasting = false;
        try {
            Field f = this.getFilteredEntityClass().getDeclaredField(this.getAttributeName());
            needsCasting = !f.getType().isAssignableFrom(String.class);
        } catch (NoSuchFieldException e) {
            String msg = "No existe el atributo " + this.getPath() + " en la clase " + this.getFilteredEntityClass().getCanonicalName();
            //log.error(msg);
            log.info(msg);
            throw new RuntimeException(msg);
        }
        //String castExpr = (needsCasting) ? "::text " : "";
        String castPrefix = (needsCasting) ? " cast( " : "";
        String castExpr = (needsCasting) ? " as text )" : "";

        if (this.getEquals() != null) {
            conditions.add("lower(unaccent(" + castPrefix + this.getPath() + castExpr + ")) == unaccent(" + this.getEquals().toLowerCase() + ")");
        }
        if (this.getNotEquals() != null) {
            conditions.add("lower(unaccent(" + castPrefix + this.getPath() + castExpr + ")) <> unaccent(" + this.getNotEquals().toLowerCase() + ")");
        }
        if (this.getLike() != null) {
            conditions.add("lower(unaccent(" + castPrefix + this.getPath() + castExpr + ")) LIKE unaccent('%" + this.getLike().toLowerCase() + "%'" + ")");
        }
        if (this.getNotLike() != null) {
            conditions.add("lower(unaccent(" + castPrefix + this.getPath() + castExpr + ")) NOT LIKE unaccent('%" + this.getNotLike().toLowerCase() + "%'" + ")");
        }
        if (this.isNull()) {
            conditions.add("lower(" + castPrefix + this.getPath() + castExpr + ") IS NULL");
        }
        if (this.isNotNull()) {
            conditions.add("lower(" + castPrefix + this.getPath() + castExpr + ") IS NOT NULL");
        }

        String result = "";
        for (int i = 0; i < conditions.size(); i++) {
            if (i == conditions.size() - 1) {
                result += conditions.get(i);
            } else {
                result += conditions.get(i) + " AND ";
            }
        }
        return result;
    }

    @Override
    public void setGeneralCondition(String condition) {
        if (condition != null) {
            this.setLike(condition);
        }

    }

    @Override
    public String parse(String parse) {

        return parse;
    }

    @Override
    public Boolean isEmpty() {
        return (super.isEmpty() && (this.getLike() == null || this.getLike().isEmpty()) && (this.getNotLike() == null || this.getNotLike().isEmpty()));
    }
}