package com.app.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.xml.transform.Source;

import org.dmg.pmml.FieldName;
import org.dmg.pmml.PMML;
import org.dmg.pmml.TreeModel;
import org.jpmml.evaluator.Evaluator;
import org.jpmml.evaluator.FieldValue;
import org.jpmml.evaluator.ModelEvaluator;
import org.jpmml.evaluator.NodeScoreDistribution;
import org.jpmml.evaluator.TreeModelEvaluator;
import org.jpmml.model.ImportFilter;
import org.jpmml.model.JAXBUtil;
import org.xml.sax.InputSource;

import com.app.domain.ConjuntoDato;
import com.app.domain.Prediccion;
import com.app.domain.Reporte;
import com.app.persistence.ConjuntoDatoDAO;
import com.app.persistence.PrediccionDAO;

public class PredictionHelper {

	@Inject
	PrediccionDAO prediccionDao;

	@Inject
	ConjuntoDatoDAO conjuntoDatosDao;

	public static void main(String[] args) {

		PMML pmml;

		try {
			InputStream is = new FileInputStream(
					new File(
							"/home/opendata/src/main/resources/model.pmml"));
			Source transformedSource = ImportFilter.apply(new InputSource(is));
			pmml = JAXBUtil.unmarshalPMML(transformedSource);

			ModelEvaluator<TreeModel> modelEvaluator = new TreeModelEvaluator(
					pmml);
			Evaluator evaluator = (Evaluator) modelEvaluator;

			List<FieldName> activeFields = evaluator.getActiveFields();
			List<FieldName> targetFields = evaluator.getTargetFields();
			List<FieldName> outputFields = evaluator.getOutputFields();

			for (FieldName activeField : activeFields) {
				System.out.println("activeField --------> "
						+ activeField.getValue());
			}

			for (FieldName targetField : targetFields) {
				System.out.println("targetField --------> "
						+ targetField.getValue());
			}

			for (FieldName outputField : outputFields) {
				System.out.println("outputField --------> "
						+ outputField.getValue());
			}

			Map<FieldName, FieldValue> arguments = new LinkedHashMap<FieldName, FieldValue>();

			/*
			 * for (FieldName activeField : activeFields) { // The raw (ie.
			 * user-supplied) value could be any Java primitive // value Object
			 * rawValue = "1";
			 * 
			 * // The raw value is passed through: // 1) outlier treatment, 2)
			 * missing value treatment, 3) invalid // value treatment and 4)
			 * type conversion FieldValue activeValue =
			 * evaluator.prepare(activeField, rawValue);
			 * 
			 * arguments.put(activeField, activeValue); }
			 */

			FieldName adm1_nombre = new FieldName("adm1_nombre");
			Object adm1_nombre_tmp = "SAN PEDRO";
			FieldValue adm1_nombre_val = evaluator.prepare(adm1_nombre,
					adm1_nombre_tmp);
			arguments.put(adm1_nombre, adm1_nombre_val);

			FieldName porcentaje_acceso_agua = new FieldName(
					"porcentaje_acceso_agua");
			Object porcentaje_acceso_agua_tmp = 100;
			FieldValue porcentaje_acceso_agua_val = evaluator.prepare(
					porcentaje_acceso_agua, porcentaje_acceso_agua_tmp);
			arguments.put(porcentaje_acceso_agua, porcentaje_acceso_agua_val);

			FieldName semana = new FieldName("semana");
			Object semana_tmp = new Integer(10);
			FieldValue semana_val = evaluator.prepare(semana, semana_tmp);
			arguments.put(semana, semana_val);

			FieldName brote = new FieldName("brote");
			Object brote_tmp = "SI";
			FieldValue brote_val = evaluator.prepare(brote, brote_tmp);
			arguments.put(brote, brote_val);

			// The scoring
			Map<FieldName, ?> results = evaluator.evaluate(arguments);

			FieldName targetName = evaluator.getTargetField();

			Object targetValue = results.get(targetName);
			System.out.println("targetValue result --------> "
					+ targetValue.toString());

			NodeScoreDistribution nodeScore = (NodeScoreDistribution) targetValue;
			System.out.println("prediction ---------> "
					+ nodeScore.getResult().toString());

			/*
			 * if (targetValue instanceof HasEntityId) { HasEntityId hasEntityId
			 * = (HasEntityId) targetValue; HasEntityRegistry<?>
			 * hasEntityRegistry = (HasEntityRegistry<?>) evaluator;
			 * BiMap<String, ? extends Entity> entities = hasEntityRegistry
			 * .getEntityRegistry(); Entity winner =
			 * entities.get(hasEntityId.getEntityId());
			 * System.out.println("winnerId ----------> " + winner.getId()); if
			 * (targetValue instanceof HasProbability) { HasProbability
			 * hasProbability = (HasProbability) targetValue; Double
			 * winnerProbability = hasProbability
			 * .getProbability(winner.getId());
			 * System.out.println("winnerProbability ----------> " +
			 * winnerProbability); } }
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String predict(ConjuntoDato conjunto) {
		PMML pmml;

		try {
			InputStream is = new FileInputStream(
					new File(
							"/home/opendata/src/main/resources/model.pmml"));
			Source transformedSource = ImportFilter.apply(new InputSource(is));
			pmml = JAXBUtil.unmarshalPMML(transformedSource);

			ModelEvaluator<TreeModel> modelEvaluator = new TreeModelEvaluator(
					pmml);
			Evaluator evaluator = (Evaluator) modelEvaluator;

			List<FieldName> activeFields = evaluator.getActiveFields();
			List<FieldName> targetFields = evaluator.getTargetFields();
			List<FieldName> outputFields = evaluator.getOutputFields();

			for (FieldName activeField : activeFields) {
				System.out.println("activeField --------> "
						+ activeField.getValue());
			}

			for (FieldName targetField : targetFields) {
				System.out.println("targetField --------> "
						+ targetField.getValue());
			}

			for (Prediccion prediccion : conjunto.getDatos()) {
				Map<FieldName, FieldValue> arguments = new LinkedHashMap<FieldName, FieldValue>();

				if (prediccion.getBrote() != null) {
					Object attBroteTmp = prediccion.getBrote();
					FieldName keyBroteAtt = new FieldName("brote");
					FieldValue attBroteVal = evaluator.prepare(keyBroteAtt,
							attBroteTmp);
					arguments.put(keyBroteAtt, attBroteVal);
				}

				if (prediccion.getCantidad() != null) {
					Object attCantidadTmp = prediccion.getCantidad();
					FieldName keyCantidadAtt = new FieldName("cantidad_total");
					FieldValue attCantidadVal = evaluator.prepare(
							keyCantidadAtt, attCantidadTmp);
					arguments.put(keyCantidadAtt, attCantidadVal);
				}

				if (prediccion.getIncidencia() != null) {
					Object attIncidenciaTmp = prediccion.getIncidencia();
					FieldName keyIncidenciaAtt = new FieldName("incidencia");
					FieldValue attIncidenciaVal = evaluator.prepare(
							keyIncidenciaAtt, attIncidenciaTmp);
					arguments.put(keyIncidenciaAtt, attIncidenciaVal);
				}

				if (prediccion.getCantidadAnterior() != null) {
					Object attCantAnteriorTmp = prediccion.getCantidadAnterior();
					FieldName keyCantAnteriorAtt = new FieldName(
							"cantidadanterior");
					FieldValue attCantAnteriorVal = evaluator.prepare(
							keyCantAnteriorAtt, attCantAnteriorTmp);
					arguments.put(keyCantAnteriorAtt, attCantAnteriorVal);
				}

				if (prediccion.getCantidadAnteAnterior() != null) {
					Object attCantAnteAnteriorTmp = prediccion.getCantidadAnteAnterior();
					FieldName keyCantAnteAnteriorAtt = new FieldName(
							"cantanteanterior");
					FieldValue attCantAnteAnteriorVal = evaluator.prepare(
							keyCantAnteAnteriorAtt, attCantAnteAnteriorTmp);
					arguments.put(keyCantAnteAnteriorAtt,
							attCantAnteAnteriorVal);
				}

				// The scoring
				Map<FieldName, ?> results = evaluator.evaluate(arguments);
				FieldName targetName = evaluator.getTargetField();

				Object targetValue = results.get(targetName);
				System.out.println("targetValue result --------> "
						+ targetValue.toString());
				NodeScoreDistribution nodeScore = (NodeScoreDistribution) targetValue;
				System.out.println("prediction ---------> "
						+ nodeScore.getResult().toString());

				prediccion.setBroteSgte(nodeScore.getResult().toString());
			}

			// Persistir la prediccion en la base de datos
			conjuntoDatosDao.insertConjunto(conjunto);

			return conjunto.getDatos().get(0).getBroteSgte();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void asignAttToConjunto(ConjuntoDato conjunto, String key,
			Object value) {
		switch (key) {
		case "semana":
			conjunto.setSemana((String) value);
			break;
		case "anio":
			conjunto.setAnio((String) value);
			break;
		case "usuario_alta":
			conjunto.setUsuarioAlta((String) value);
			break;
		default:
			break;
		}
	}

	private void asignAttToPrediction(Prediccion prediccion, String key,
			String value) {
		switch (key) {
		case "cantidad_total":
			prediccion.setCantidad(Long.parseLong(value));
			break;
		case "incidencia":
			prediccion.setIncidencia(Double.parseDouble(value));
			break;
		case "cantidadanterior":
			prediccion.setCantidadAnterior(Long.parseLong(value));
			break;
		case "cantanteanterior":
			prediccion.setCantidadAnteAnterior(Long.parseLong(value));
			break;
		case "brote":
			prediccion.setBrote((String) value);
			break;
		case "departamento":
			prediccion.setAdm1Nombre((String) value);
			break;
		case "distrito":
			prediccion.setAdm2Nombre((String) value);
			break;
		case "barrio":
			prediccion.setAdm3Nombre((String) value);
			break;
		default:
			break;
		}
	}

	public ConjuntoDato createConjuntoIndividual(Map<String, String> map,
			ConjuntoDato conjunto) {
		Prediccion prediccion = new Prediccion();

		for (String key : map.keySet()) {
			System.out.println("CONJUNTO - evaluando el att " + key);
			asignAttToConjunto(conjunto, key, map.get(key));
		}

		for (String key : map.keySet()) {
			System.out.println("PREDICCION - evaluando el att " + key);
			asignAttToPrediction(prediccion, key, map.get(key));
		}

		List<Prediccion> prediccionList = new ArrayList<Prediccion>();
		prediccionList.add(prediccion);
		conjunto.setDatos(prediccionList);
		return conjunto;
	}

	public String createConjuntoCsv(byte[] bytes, ConjuntoDato conjunto) {
		System.out.println("++++++++++++++++++ validateFileAndSave");
		boolean valid = true;
		BufferedReader br;
		String fila;
		InputStream is = null;
		List<Prediccion> prediccionList = new ArrayList<Prediccion>();
		try {
			is = new ByteArrayInputStream(bytes);
			br = new BufferedReader(new InputStreamReader(is));
			String header = br.readLine();
			System.out.println("++++++++++++++++++ header " + header);
			if (header != null
					&& header.length() > 0
					&& header
							.compareTo("anio, semana, departamento, distrito, barrio, cantidad_total, cantidadanterior, cantanteanterior, incidencia, brote") == 0) {

				while ((fila = br.readLine()) != null) {
					String[] celdas = fila.split(",");
					Prediccion prediccion = new Prediccion();
					for (int i = 0; i < celdas.length; i++) {
						String celda = celdas[i];
						switchCelda(i, celda, conjunto, prediccion);
						// si no paso alguna de las validaciones retorna false
						if (!valid)
							return "1";
						prediccionList.add(prediccion);
					}
				}
				conjunto.setDatos(prediccionList);
			} else {
				// si el csv no tiene las columnas correctas
				return "2";
			}
			br.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("No se encuentra el archivo.");
			return "4";
		} catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("Error de entrada/salida.");
			return "4";
		}
		return "0";

	}

	public void switchCelda(int i, String celda, ConjuntoDato cd, Prediccion p) {
		switch (i) {
		case 0: // anio
			asignAttToConjunto(cd, "anio", celda);
			break;
		case 1: // semana
			asignAttToConjunto(cd, "semana", celda);
			break;
		case 2: // dpto
			asignAttToPrediction(p, "departamento", celda);
			break;
		case 3: // distrito
			asignAttToPrediction(p, "distrito", celda);
			break;
		case 4: // barrio
			asignAttToPrediction(p, "barrio", celda);
			break;
		case 5: // cantidad total
			asignAttToPrediction(p, "cantidad_total", celda);
			break;
		case 6: // cantidad anterior
			asignAttToPrediction(p, "cantidadanterior", celda);
			break;
		case 7: // cantidad ante anterior
			asignAttToPrediction(p, "cantanteanterior", celda);
			break;
		case 8: // incidencia
			asignAttToPrediction(p, "incidencia", celda);
			break;
		case 9: // brote
			asignAttToPrediction(p, "brote", celda);
			break;
		default:
			break;
		}
	}
}
