package com.app.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatter {
	public String fromDate(Date fecha) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String date = null;
		try {
			date = formatter.format(fecha);
		} catch (Exception e) {
			date = null;
		}

		return date;
	}

	public Date toDate(String d) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		try {
			date = formatter.parse(d);
		} catch (Exception e) {
			date = null;
		}

		return date;
	}

	public String getFechaFormateada(String fecha) {

		SimpleDateFormat formato = new SimpleDateFormat(
				"d 'de' MMMM 'de' yyyy", new Locale("es", "ES"));

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		formatter = new SimpleDateFormat("dd-MM-yyyy");
		try {
			date = formatter.parse(fecha);
		} catch (Exception e) {
			date = null;
		}
		if (date != null) {
			String fechaFormateada = formato.format(date);
			return fechaFormateada;
		} else {
			return "";
		}
	}
}
