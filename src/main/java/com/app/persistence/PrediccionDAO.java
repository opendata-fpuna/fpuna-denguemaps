package com.app.persistence;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;

import com.app.domain.Prediccion;
import com.app.domain.Reporte;

@Stateless
public class PrediccionDAO {

	private final static Logger logger = Logger.getLogger(PrediccionDAO.class
			.getName());

	@Inject
	EntityManager em;

	public List<Prediccion> getPredicciones() {

		Session session = (Session) em.getDelegate();
		Criteria criteria = session.createCriteria(Prediccion.class);

		return criteria.list();

		/*
		 * String query = "select * from client"; return
		 * em.createNativeQuery(query).getResultList();
		 */
	}

	@Transactional
	public boolean insertPrediccion(Prediccion prediccion) {
		logger.info("++++++++++++++++++ insertPrediccion");
		// agregar unique sobre todas las columnas excepto el id en la tabla
		// reporte porque no pueden existir dos filas iguales
		// si no se puede insertar que hacer?
		try {
			System.out.println(prediccion);
			em.persist(prediccion);
			logger.info("Insertando prediccion en la BD...");
			return true;
		} catch (Exception e) {
			// e.printStackTrace();
			logger.info("Ocurrió un error al intentar insertar la prediccion.");
			return false;
		}

	}

}
