package com.app.persistence;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.postgresql.util.PSQLException;

import com.app.domain.Notificacion;
import com.app.domain.Reporte;

@Stateless
public class ReporteDAO {

	private final static Logger logger = Logger.getLogger(ReporteDAO.class.getName());
	
	@Inject
	EntityManager em;
	
	public List<Notificacion> getReportes() {

		Session session = (Session) em.getDelegate();
		Criteria criteria = session.createCriteria(Reporte.class);

		return criteria.list();

		/*
		 * String query = "select * from client"; return
		 * em.createNativeQuery(query).getResultList();
		 */
	}

	public List<Reporte> getReportesPorAnio(String anio) {

		Query q = em.createQuery("select r from Reporte r where r.anio='"
				+ anio + "'");
		try {
			List<Reporte> listaevento = (List<Reporte>) q.getResultList();
			return listaevento;
		} catch (NoResultException e) {
			return null;
		}
	}

	@Transactional
	public boolean insertReporte(Reporte reporte) throws PSQLException {
		logger.info("++++++++++++++++++ insertReporte");
		// agregar unique sobre todas las columnas excepto el id en la tabla
		// reporte porque no pueden existir dos filas iguales
		// si no se puede insertar que hacer?
		//try {
			System.out.println(reporte);
			System.out.println("++++++ anio " + reporte.getAnio());
			System.out.println("++++++ semana " + reporte.getSemana());
			System.out.println("++++++ mes " + reporte.getMes());
			System.out.println("++++++ dia " + reporte.getDia());
			System.out.println("++++++ dpto " + reporte.getAdm1Nombre());
			System.out.println("++++++ distrito " + reporte.getAdm2Nombre());
			System.out.println("++++++ barrio " + reporte.getAdm3Nombre());
			System.out.println("++++++ grupoedad " + reporte.getGrupo_edad());
			System.out.println("++++++ sexo " + reporte.getSexo());
			System.out.println("++++++ origen " + reporte.getOrigen());
			em.persist(reporte);
			logger.info("Actualizando reporte...");
			return true;
		//} catch (Exception e) {
			//e.printStackTrace();
			//logger.info("Ocurrió un error al intentar actualizar el reporte.");
			//return false;
		//}

	}

}
