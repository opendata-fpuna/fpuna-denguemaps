package com.app.constant;

/*
 * Esta clase cuenta con todas las constantes del sistema.
 * 
 */
public class Statics {

	/** QUERYS **/
	public final static String RIESGOS_ASU = "select * from riesgosAsuncion where anio = '{0}'"; 
	
	public final static String REISGOS_DITRITOS = "select * from riesgosDistritos where anio = '{0}'" ;
	
	public final static String RIESGOS_DEPTO = "select * from riesgosDepartamentos where anio = '{0}'";
	
	public final static String NOTIF_FILTRADAS_MAP = "select sum(cantidad) as cantidad, adm1_nombre as departamento, semana, anio from reporte where anio = '{0}' {1} group by anio, semana, departamento order by semana";

	public final static String NOTIF_POR_ANIO = "select noti.departamento, CAST(coalesce(noti.semana, '0') AS integer) as semana, count(*) as cantidad from notificacion noti where anio='{0}' and semana not like '#VALUE!' group by departamento, semana order by CAST(coalesce(noti.semana, '0') AS integer), departamento";
	
	public final static String REPORTE_POR_ANIO = "select * from reporte r where anio='{0}'";
	
	public final static String INCIDENCIA_DEPTO = "select departamento, semana, incidencia, incidencia_confirmados, incidencia_sospechosos, casos, casos_sospechosos, casos_confirmados from incidenciasDepartamentos where anio = '{0}'";
	
	public final static String INCIDENCIA_DISTRITOS = "select distrito, semana, incidencia, incidencia_confirmados, incidencia_sospechosos, casos, casos_sospechosos, casos_confirmados from incidenciasDistritos where anio = '{0}'";
	
	public final static String INCIDENCIA_ASUNCION = "select barrio, semana, incidencia, incidencia_confirmados, incidencia_sospechosos, casos, casos_sospechosos, casos_confirmados from incidenciasasuncion where anio = '{0}'";
	
	public final static String POBLACION_DEPTO = "select departamento_codigo, departamento_nombre, total from poblacion where anio = '{0}' and (distrito_codigo is null or distrito_codigo = '0') order by departamento_codigo::integer";
	
	public final static String POBLACION_DISTRITO = "select departamento_codigo, departamento_nombre, distrito_codigo, distrito_nombre, total from poblacion where anio = '{0}' and (distrito_codigo is not null or distrito_codigo != '0')";
	
	public final static String POBLACION_ASUNCION = "select barrio_nombre, barrio_codigo, poblacion as total from poblacion_asuncion where barrio_codigo is not null";
	
	public final static String CONJUNTOS_DATOS = "select id, nombre, anio, semana from conjunto_dato where nombre not like 'Predicción individual'";
	
	public final static String CONJUNTOS_DATOS_ANIO = "select id, nombre, anio, semana from conjunto_dato where anio = '{0}' and nombre not like 'Predicción individual' order by semana";
	
	public final static String PRED_DISTRITOS = "select d.departamento_descripcion || '-' || d.distrito_descripcion as distrito," + 
			" case when p.brote is not null then p.brote else '' end as brote" +
			" from distrito d left join prediccion p on (d.distrito_descripcion=p.adm2_nombre and p.id_conjunto_dato = {0})" + 
			" order by d.departamento_descripcion, d.distrito_descripcion";
	
	public final static String PRED_BARRIOS = "select b.distrito_descripcion || '-' || b.barrio_descripcion as barrio," +
			" case when p.brote is not null then p.brote else '' end as brote" +
			" from barrio b left join prediccion p on (b.barrio_descripcion=p.adm3_nombre" + 
			" and p.id_conjunto_dato = {0} " + 
			" and p.adm3_nombre is not null" + 
			" and p.adm3_nombre not like '')" + 
			" where b.departamento_descripcion = 'ASUNCION' and b.distrito_descripcion = 'ASUNCION'" +
			" order by b.distrito_descripcion, b.barrio_descripcion";

}
