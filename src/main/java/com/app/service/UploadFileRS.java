package com.app.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.app.domain.Reporte;
import com.app.persistence.ParametrosDAO;
import com.app.persistence.ReporteDAO;
import com.app.utils.DateFormatter;
import com.app.utils.FileManager;
import org.postgresql.util.PSQLException;

@Path("/rest/file")
public class UploadFileRS {
	
	private final static Logger logger = Logger.getLogger(UploadFileRS.class.getName());
	
	private final String UPLOADED_FILE_PATH = "/tmp/";
	
	@Inject
	DateFormatter dateFormatter;
	
	@Inject
	ParametrosDAO parametrosDao;
	
	@Inject
	ReporteDAO reporteDao;
	
	@Inject
	FileManager fileManager;
	 
	@POST
	//@GET
	@Path("/upload")
	@Consumes("multipart/form-data")
	public Response uploadFile(MultipartFormDataInput input) {
 
		logger.info("++++++++++++++++++++ input " + input);
		String fileName = "";
 
		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		logger.info("+++++++++++ uploadForm" + uploadForm);
		List<InputPart> inputParts = uploadForm.get("file");
 
		for (InputPart inputPart : inputParts) {

			try {
				MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = fileManager.getFileName(header);
	 
				//convert the uploaded file to inputstream
				InputStream inputStream = inputPart.getBody(InputStream.class,null);
	 
				byte [] bytes = IOUtils.toByteArray(inputStream);
	 			fileName = UPLOADED_FILE_PATH + fileName;
	
				String[] aux = fileName.split("\\.");
				String extension = aux[aux.length - 1];
				
				if (extension.compareTo("csv") == 0) {
					String response = validateFileAndSave(bytes);
					switch (response) {
					case "0":
						fileManager.writeFile(bytes,fileName + dateFormatter.fromDate(new Date()));
						logger.info("Done");
						break;
					case "1":
						return Response.ok("{\"file\": \"contenido_de_filas_invalido\"}").build();
					case "2":
						return Response.ok("{\"file\": \"estructura_de_columnas_invalida\"}").build();
					case "3":
						return Response.ok("{\"file\": \"contenido_duplicado\"}").build();
					default:
						return Response.ok("{\"file\": \"error_interno\"}").build();
					}
				} else {
					return Response.ok("{\"file\": \"formato_invalido\"}").build();
				}
 
			} catch (IOException e) {
				e.printStackTrace();
				return Response.status(500).entity("{\"file\": \"error_interno\"}").build();
			}
 
		}
		return Response.ok("{\"file\": \"carga_exitosa\"}").build();
 
	}
	
	private static String[] regionesValidas = new String[] { "AMERICA DEL SUR" };
	private static String[] paisesValidos = new String[] { "PARAGUAY" };
	private static String[] sexosValidos = new String[] { "M", "F" };
	private static String[] edadesValidas = new String[] { "< 5", "5 - 9",
			"10 - 19", "20 - 59", ">= 60" };
	private static String[] origenesValidos = new String[] { "IMPORTADO",
			"AUTOCTONO" };
	private static String[] estadosFinalesValidos = new String[] {
			"CONFIRMADO", "SOSPECHOSO", "DESCARTADO", "MUERTE" };
	private static String[] estadosClinicosValidos = new String[] { "DF",
			"DHF", "DSS", "A", "B", "C" };

	private static List<String> departamentosValidos;
	private static List<String> distritosValidos;
	private static List<String> barriosValidos;
	
	/** Valida que el archivo cumpla con el esquema **/
	@Transactional
	public String validateFileAndSave(byte[] bytes) {
		logger.info("++++++++++++++++++ validateFileAndSave");
		departamentosValidos = parametrosDao.getDepartamentos();
		distritosValidos = parametrosDao.getDistritos();
		barriosValidos = parametrosDao.getBarrios();
		boolean valid = true;
		BufferedReader br;
		String fila;
		InputStream is = null;
		List<Reporte> reportesList = new ArrayList<Reporte>();
		try {
			is = new ByteArrayInputStream(bytes);
			br = new BufferedReader(new InputStreamReader(is));
			String header = br.readLine();
			System.out.println("++++++++++++++++++ header " + header);
			if (header != null
					&& header.length() > 0
					&& header
							.compareTo("cantidad,anio,mes,dia,semana,region,pais,adm1,adm2,adm3,sexo,grupo_edad,estado_final,clasificacion_clinica,serotipo,origen,fuente") == 0) {
				while ((fila = br.readLine()) != null) {
					//logger.info("================= Dentro del while");
					String[] celdas = fila.split(",");
					for (int i = 0; i < celdas.length; i++) {
						String celda = celdas[i];
						valid = switchCelda(i,celda);
						// si no paso alguna de las validaciones retorna false
						if (!valid)
							return "1";
					}
					// si paso todas las validaciones se inserta la lista
					reportesList.add(createReporte(fila));
				}
				// se insertan los nuevos reportes en la BD
				for (Reporte reporte : reportesList) {
					try {
						reporteDao.insertReporte(reporte);
					} catch (PSQLException e) {
						//e.printStackTrace();
						return "3";
					}
				}
				//reporteDao.insertReportes(reportesList);
			} else {
				// si el csv no tiene las columnas correctas
				return "2";
			}
			br.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.info("No se encuentra el archivo.");
			return "4";
		} catch (IOException ex) {
			ex.printStackTrace();
			logger.info("Error de entrada/salida.");
			return "4";
		}
		return "0";
	}
	
	public boolean switchCelda(int i, String celda){
		switch (i) {
		case 0: // cantidad
			//Integer cantidad = Integer.getInteger(celda);
			logger.info("================= cantidad " + celda);
			if (celda == null || celda.compareTo("0") == 0) {
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 1: // anio
			logger.info("================= anio " + celda);
			if (celda == null || celda.compareTo("") == 0) {
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 2: // mes
			Integer mes = Integer.parseInt(celda);
			logger.info("================= mes " + mes);
			if (mes == null || mes < 0 || mes > 12) {
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 3: // dia
			Integer dia = Integer.parseInt(celda);
			logger.info("================= dia " + dia);
			if (dia == null || dia < 0 || dia > 31) {
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 4: // semana
			Integer semana = Integer.parseInt(celda);
			logger.info("================= semana " + semana);
			if (semana == null || semana < 0 || semana > 54) {
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 5: // region
			logger.info("================= region " + celda);
			if (!existe(celda, regionesValidas)){
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 6: // pais
			logger.info("================= pais " + celda);
			if (!existe(celda, paisesValidos)){
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 7: // departamento
			logger.info("================= departamento " + celda);
			if (!departamentosValidos.contains(celda)) {
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 8: // distrito
			logger.info("================= distrito " + celda);
			if (!distritosValidos.contains(celda)) {
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 9: // barrio - puede ser null
			logger.info("================= barrio " + celda);
			if (celda != null && !barriosValidos.contains(celda)) {
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 10: // sexo
			logger.info("================= sexo " + celda);
			if (!existe(celda, sexosValidos)){
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 11: // grupo edad
			logger.info("================= edad " + celda);
			if (!existe(celda, edadesValidas)){
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 12: // estado_final
			logger.info("================= estado " + celda);
			if (!existe(celda, estadosFinalesValidos)){
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 13: // clasificacion_clinica
			logger.info("================= clasificacion " + celda);
			if (celda != null && celda.compareTo("")==0 && !existe(celda, estadosClinicosValidos) && !existe(celda, estadosClinicosValidos)){
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 14: // serotipo - puede ser null
			return true;
		case 15: // origenes
			logger.info("================= origen " + celda);
			if (!existe(celda, origenesValidos)) {
				logger.info("================= invalido");
				return false;
			}
			return true;
		case 16: // fuente - puede ser null
			return true;
		default:
			logger.info("================= default");
			return true;
		}
	}

	public boolean existe(String celda, String[] list) {
		boolean existe = false;
		if (celda == null || celda.compareTo("") == 0) {
			return false;
		} else {
			for (String string : list) {
				if (string.compareTo(celda) == 0) {
					existe = true;
				}
			}
		}
		return existe;
	}
	
	public Reporte createReporte(String fila) {
		String[] celdas = fila.split(",");
		Reporte reporte = new Reporte();
		Long cantidad = Long.parseLong(celdas[0]);
		reporte.setCantidad(cantidad);
		reporte.setAnio(celdas[1]);
		Integer mes = Integer.parseInt(celdas[2]);
		reporte.setMes(12);
		Integer dia = Integer.parseInt(celdas[3]);
		reporte.setDia(22);
		reporte.setSemana(celdas[4]);
		reporte.setRegion(celdas[5]);
		reporte.setPais(celdas[6]);
		reporte.setAdm1Nombre(celdas[7]);
		reporte.setAdm2Nombre(celdas[8]);
		reporte.setAdm3Nombre(celdas[9]);
		reporte.setSexo(celdas[10]);
		reporte.setGrupo_edad(celdas[11]);
		reporte.setEstado_final(celdas[12]);
		reporte.setClasificacion_clinica(celdas[13]);
		reporte.setSerotipo(celdas[14]);
		reporte.setOrigen(celdas[15]);
		reporte.setFuente(celdas[16]);
		return reporte;
	}

	public boolean validateFile(byte[] bytes) {
		// logger.info("++++++++++++++++++ validateFile");
		boolean valid = true;
		BufferedReader br;
		String strLine;
		InputStream is = null;
		try {
			is = new ByteArrayInputStream(bytes);
			br = new BufferedReader(new InputStreamReader(is));
			String header = br.readLine();
			logger.info("++++++++++++++++++ header " + header);
			if (header != null && header.length() > 0) {
				if (header
						.compareTo("cantidad,anio,mes,dia,semana,region,pais,adm1,adm2,adm3,sexo,grupo_edad,estado_final,clasificacion_clinica,serotipo,id") == 0) {
					while ((strLine = br.readLine()) != null) {
						String extras = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœº";
						if (strLine.length() > 0) {
							String regex = "^[0-9]+,[0-9]+,[0-9]+,[0-9]+,[0-9]+,"
									+ "[A-Za-z0-9 -\\. \\/ "
									+ extras
									+ "]+,[A-Za-z0-9 -\\. \\/ "
									+ extras
									+ "]+,[A-Za-z0-9 -\\. \\/ "
									+ extras
									+ "]*,[A-Za-zñÑ0-9 -\\. \\/ "
									+ extras
									+ "]*,[A-Za-zñÑ0-9 -\\. \\/ "
									+ extras
									+ "]*,"
									+ "[A-Za-zñÑ \\/]*,[>? <? A-Za-z0-9 -?]*,[A-Za-z\\/]*,[A-Za-zñÑ0-9 \\/]*,[A-Za-zñÑ0-9 \\/]*,[0-9]+$";
							if (!strLine.matches(regex)) {
								System.out
										.println("++++++++++++++++++ strLine "
												+ strLine);
								logger.info("pattern matched");
								valid = false;
							}
						} else {
							valid = false;
						}
					}
				} else {
					valid = false;
				}
			} else {
				valid = false;
			}
			br.close();

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			valid = false;
		} catch (IOException e) {
			e.printStackTrace();
			valid = false;
		}
		return valid;
	}
 
}
