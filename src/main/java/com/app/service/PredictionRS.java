package com.app.service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.app.constant.Statics;
import com.app.domain.ConjuntoDato;
import com.app.utils.DateFormatter;
import com.app.utils.FileManager;
import com.app.utils.NativeQueryFileManager;
import com.app.utils.PredictionHelper;

@Named
@RequestScoped
@Path("/rest/prediccion")
public class PredictionRS {

	private final static Logger logger = Logger.getLogger(PredictionRS.class
			.getName());

	@Inject
	PredictionHelper predictionHelper;

	@Inject
	DateFormatter dateFormatter;
	
	@Inject
	FileManager fileManager;
	
	@Inject
	private NativeQueryFileManager n;
	
	private final String UPLOADED_FILE_PATH = "/tmp/";

	@POST
	@Path("/individual")
	@Produces("application/json")
	public Response getPrediccionIndividual(@Context UriInfo uriInfo) {
		MultivaluedMap<String, String> parameters = uriInfo
				.getQueryParameters();
		Map<String, String> finalMap = new HashMap<String, String>();
		Set<String> keys = parameters.keySet();
		// cada uno de estos parametros es el atributo que viene del formulario
		for (String key : keys) {
			List<String> list = parameters.get(key);
			String obj = (String) list.get(0);
			finalMap.put(key, obj);
		}
		
		// si fuera csv crear de otra forma
		ConjuntoDato conjunto = new ConjuntoDato();
		conjunto.setFechaAlta(new Date());
		conjunto.setNombre("Predicción individual");
		conjunto = predictionHelper.createConjuntoIndividual(finalMap, conjunto);
		
		// Datos de ejemplo
		// CENTRAL		VILLETA	5	100.0	27.0	7.967	1.476	33891	22.0	6.491	-1.771	33891	28.0	8.262	33891	SI	SI
		// ALTO PARANA	YGUAZU	6	100.0	5.0		4.602	0.0		10866	0.0		0.0		0.0		10866	1.0		0.92	10866	NO	SI
		// ALTO PARANA	YGUAZU	7	100.0	2.0		1.841	-2.761	10866	5.0		4.602	4.602	10866	0.0		0.0		10866	NO	NO
		
		// se retorna SI o NO
		String resultado = "{\"prediccion\": \""+predictionHelper.predict(conjunto)+"\"}";
		logger.info("+++ resultado:: "+resultado);
		return Response.ok(resultado).build();
	}

	@POST
	@Path("/lista")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPrediccionCsv(MultipartFormDataInput input) {
		
		logger.info("++++++++++++++++++++ input " + input);
		String fileName = "";
 
		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		logger.info("+++++++++++ uploadForm" + uploadForm);
		List<InputPart> inputParts = uploadForm.get("file");
 
		for (InputPart inputPart : inputParts) {
			try {
				MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = fileManager.getFileName(header);
	 
				//convert the uploaded file to inputstream
				InputStream inputStream = inputPart.getBody(InputStream.class,null);
	 
				byte [] bytes = IOUtils.toByteArray(inputStream);
	 			fileName = UPLOADED_FILE_PATH + fileName;
	
				String[] aux = fileName.split("\\.");
				String extension = aux[aux.length - 1];
				
				if (extension.compareTo("csv") == 0) {
					ConjuntoDato conjunto = new ConjuntoDato();
					conjunto.setNombre(fileName);
					conjunto.setFechaAlta(new Date());
					String response = predictionHelper.createConjuntoCsv(bytes, conjunto);

					switch (response) {
					case "0":
						fileManager.writeFile(bytes,fileName + dateFormatter.fromDate(new Date()));
						predictionHelper.predict(conjunto);
						logger.info("Done");
						break;
					case "1":
						return Response.ok("{\"file\": \"contenido_de_filas_invalido\"}").build();
					case "2":
						return Response.ok("{\"file\": \"estructura_de_columnas_invalida\"}").build();	
					default:
						return Response.ok("{\"file\": \"error_interno\"}").build();
					}
				} else {
					return Response.ok("{\"file\": \"formato_invalido\"}").build();
				}
 
			} catch (IOException e) {
				e.printStackTrace();
				return Response.status(500).entity("{\"file\": \"error_interno\"}").build();
			}
		}
		return Response.ok("{\"file\": \"carga_exitosa\"}").build();
	}
	
	@GZIP
	@GET
	@Path("/conjuntos")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConjuntos() {
		logger.info("+++++++++++++++++ Servicio de conjuntos de datos");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.CONJUNTOS_DATOS);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}

	@GZIP
	@GET
	@Path("/conjuntos/{anio}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConjuntosPorAnio(@PathParam("anio") String anio) {
		logger.info("+++++++++++++++++ Servicio de conjuntos de datos por año");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.CONJUNTOS_DATOS_ANIO,
					anio);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}

	@GZIP
	@GET
	@Path("/departamentos/{conjunto}")
	@Produces("application/json")
	public Response getPrediccionesDepartamentos(
			@PathParam("conjunto") String idConjunto) {
		logger.info("+++++++++++++++++ Predicciones de departamentos");
		// lista de filas, nombre columna: valor
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> dptoList = new ArrayList<Map<String, Object>>();
		Map<String, Double> dptoCant = new HashMap<String, Double>();
		Map<String, Double> dptoCantBrote = new HashMap<String, Double>();
		try {
			list = n.executeNativeQueryParameters(Statics.PRED_DISTRITOS,
					idConjunto);
			for (Map<String, Object> map : list) {
				// System.out.println("------------- fila " + map);
				// si es un brote aumentar la cantidad del departamento
				String dpto = ((String) map.get("distrito")).split("-")[0];
				String brote = (String) map.get("brote");
				if (brote.compareTo("SI") == 0) {
					if (dptoCantBrote.get(dpto) != null) {
						dptoCantBrote.put(dpto, dptoCantBrote.get(dpto) + 1d);
					} else {
						dptoCantBrote.put(dpto, 1d);
					}
				} else {
					if (dptoCantBrote.get(dpto) != null) {
						dptoCantBrote.put(dpto, dptoCantBrote.get(dpto));
					} else {
						dptoCantBrote.put(dpto, 0d);
					}
				}
				if (dptoCant.get(dpto) != null) {
					dptoCant.put(dpto, dptoCant.get(dpto) + 1d);
				} else {
					dptoCant.put(dpto, 1d);
				}
			}
			System.out.println("--------- cantidades por departamento "
					+ dptoCant);
			System.out.println("--------- brotes por departamento "
					+ dptoCantBrote);
			// meter en la lista
			Set<String> dptos = dptoCant.keySet();
			for (String dpto : dptos) {
				Map<String, Object> dptoPorcentaje = new HashMap<String, Object>();
				dptoPorcentaje.put("departamento", dpto);
				Double porcentaje = dptoCantBrote.get(dpto) * 100d
						/ dptoCant.get(dpto);
				dptoPorcentaje.put("porcentaje",
						String.format("%.2f", porcentaje));
				dptoList.add(dptoPorcentaje);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(dptoList).build();
	}

	@GZIP
	@GET
	@Path("/distritos/{conjunto}")
	@Produces("application/json")
	public Response getPrediccionesDistritos(
			@PathParam("conjunto") String idConjunto) {
		logger.info("+++++++++++++++++ Predicciones de distritos");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.PRED_DISTRITOS,
					idConjunto);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}

	@GZIP
	@GET
	@Path("/barrios/{conjunto}")
	@Produces("application/json")
	public Response getPrediccionesBarrios(
			@PathParam("conjunto") String idConjunto) {
		logger.info("+++++++++++++++++ Predicciones de barrios");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			list = n.executeNativeQueryParameters(Statics.PRED_BARRIOS,
					idConjunto);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return Response.ok(list).build();
	}
	
}
