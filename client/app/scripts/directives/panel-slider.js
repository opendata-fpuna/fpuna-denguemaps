'use strict';
/**
 * @ngdoc directive
 * @name denguemapsApp.directive:panelSlider
 * @description
 * # slider
 */


angular.module('denguemapsApp')
  .directive('panelSlider', function ($rootScope,  $http, mapUtil) {
    return {
		restrict: 'E',
		replace: false,
		scope: {
			detalle:'='
		},
        templateUrl: 'views/templates/panel-slider.html',
    	link: function postLink(scope, element, attrs) {
			
			var mapSlider;
			var selectedYear = 2013;
			var selectedWeek = 1;

			$("#sYear").noUiSlider({
				start: 2013,
				direction: "ltr",
				step: 1,
				range: {
					'min': 2009,
					'max': 2013
				},
				format: wNumb({
					decimals: 0
				})
			});
			$('#sYear').noUiSlider_pips({
				mode: 'steps',
				density: 4
			});

			$("#sMonth").noUiSlider({
				start: 25,
				step: 1,
				direction: "ltr",
				range: {
					'min': 1,
					'max': 53
				},
				format: wNumb({
					decimals: 0
				})
			});
			$('#sMonth').noUiSlider_pips({
				mode: 'count',
				values: 6,
				density: 4
			});


			$("#sYear").Link('lower').to($("#fYear"));
			$("#sMonth").Link('lower').to($("#fMonth"));

			/*$("#sMonth").on({
				change: function(){
					mapUtil.reloadSem($("#sMonth").val());
				}
			});

			$("#sYear").on({
				change: function(){
					mapUtil.reloadAnio($("#sMonth").val(), $("#sYear").val());
				}
			});*/

	    }
	};
});