'use strict';

/**
 * @ngdoc directive
 * @name coreApp.directive:genericDatatable
 * @description
 * # genericDatatable
 */
angular.module('denguemapsApp')
  .directive('genericDatatable', function ($resource, $location, DTOptionsBuilder, DTColumnBuilder) {
    return {
      templateUrl: 'views/templates/generic-datatable.html',
      restrict: 'AE',
      replace: true,
      scope: {
        options: '='
      },
      controller: function postLink($scope, $element) {
        var urlTemplate = _.template(window.basePath + '/denguemaps-server/rest/<%= resource %>/lista?');
        var format = $scope.options.format || 'application/json';
        $scope.dtInstance = {};
        var ajaxRequest = function(data, callback) {
          // TODO: mandar esto al widget de datatable generico
          var xhr = $resource(urlTemplate($scope.options) + $.param(data), {}, {
            query: {
              isArray: false
            },
            get: {
              method: 'GET',
              headers: { 'Accept': format }
            }
          });
          xhr.query().$promise.then(function(response) {
            callback(response);
          });
        };
        var ajaxConfig = ($scope.options.ajax) ? $scope.options.ajax : ajaxRequest;

        $scope.dtOptions = DTOptionsBuilder.newOptions()
          .withOption('ajax', ajaxConfig)
          .withDataProp('data')
          .withOption('processing', true)
          .withOption('serverSide', true)
          .withOption('language', {
                  "sProcessing" : "Procesando...",
                  "sLengthMenu" : "Mostrar _MENU_ registros",
                  "sZeroRecords" : "No se encontraron resultados",
                  "sEmptyTable" : "Ningún dato disponible en esta tabla",
                  "sInfo" : "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                  "sInfoEmpty" : "Mostrando registros del 0 al 0 de un total de 0 registros",
                  "sInfoFiltered" : "(filtrado de un total de _MAX_ registros)",
                  "sInfoPostFix" : "",
                  "sSearch" : "Buscar:",
                  "sUrl" : "",
                  "sInfoThousands" : ",",
                  "sLoadingRecords" : "Cargando...",
                  "oPaginate" : {
                    "sFirst" : "Primero",
                    "sLast" : "Último",
                    "sNext" : "Siguiente",
                    "sPrevious" : "Anterior"
                  },
                  "oAria" : {
                    "sSortAscending" : ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending" : ": Activar para ordenar la columna de manera descendente"
                  }
                })
          .withPaginationType('full_numbers')
          .withBootstrap();

        $scope.visibleColumns = $scope.options.columns.length;

        $scope.dtColumns = _.map($scope.options.columns, function(c){
          var column = DTColumnBuilder.newColumn(c.data);
          var commonAttrs = ['data', 'title', 'class', 'renderWith', 'visible', 'sortable']
          if(c.title) column = column.withTitle(c.title);
          if(c.class) column = column.withClass(c.class);
          if(c.renderWith) column = column = column.renderWith(c.renderWith);
          if(c.visible === false) {
            column = column.notVisible();
            $scope.visibleColumns -= 1;
          }
          if(c.sortable === false) column = column.notSortable();
          _.forOwn(c, function(value, key){
            if(!_.contains(commonAttrs, key)){
              column = column.withOption(key, value);
            }
          });
          return column;
        });

        /*$scope.new = function(){
          var pathTemplate = _.template('/<%= resource %>/new');
          $location.path(pathTemplate($scope.options));
        }*/

        /* FALTA ver como agregar los filtros solo segun la cantidad de columnas visibles */
        $scope.dtInstanceCallback = function(dtInstance){
          console.log($scope.dtVisibleColumns);
          var tableId = dtInstance.id;

          for (var i = 0; i < $scope.visibleColumns; i++) {
            $('#' + tableId + ' tfoot tr').append('<th></th>');
          }

          // Setup - add a text input to each footer cell
          $('#' + tableId + ' tfoot th').each(
            function() {
              var title = $('#' + tableId + ' thead th').eq($(this).index()).text();
              $(this).html(
                  '<input class="column-filter form-control input-sm" type="text" placeholder="'
                      + title + '" style="width: 100%;" />');
          });

          $('tfoot').insertAfter('thead');
          var table = dtInstance.DataTable;
          table.columns().eq(0).each(
            function(colIdx) {
              $('tfoot input:eq(' + colIdx.toString() + ')').on('keyup change',
                  function(e) {
                      var realIndex;
                      var that = this;
                      _.each($scope.dtColumns, function(object, index) {
                          if (object.sTitle == that.placeholder) {
                              realIndex = index;
                          }
                      });
                      var index = realIndex || colIdx;
                      if(this.value.length >= 1 || e.keyCode == 13){
                        table.column(index).search(this.value).draw();
                      }
                      // Ensure we clear the search if they backspace far enough
                      if(this.value == "") {
                          table.column(index).search("").draw();
                      }
                  });
          });
        }
      }
    };
  });
