'use strict';
/**
 * @ngdoc directive
 * @name denguemapsApp.directive:map
 * @description
 * # slider
 */


angular.module('denguemapsApp')
  .directive('mapaAlertas', function ($rootScope,  $http, mapServices, $q) {
    return {
		restrict: 'E',
		replace: false,
		require: '^map',
		scope: {
			detalle:'='
		},
		templateUrl: 'views/templates/mapa-alertas.html',
    	link: function postLink(scope, element, attrs, mapCtrl) {
    		mapCtrl.imprimir("mapaAlertas");
			mapCtrl.cleanMap();

			var departamentos;
			var map = mapCtrl.getMap();
			var STATE = $rootScope.ALERTA_STATE;
			var departamentos = mapCtrl.getDepartamentosPrincipales();
			console.log("STATE::");
			console.log($rootScope.ALTERTA_STATE);
			scope.listaAnios = ['-','2009', '2010', '2011', '2012', '2013','2014','2015','2016','2017'];
			scope.anioSelect = '-';
			if(STATE === undefined){
				$rootScope.ALTERTA_STATE = {}
				STATE = {};
				mapServices.getListaConjuntos()
				.then(function(data){
					scope.conjuntos = data;
					STATE.conjuntos = scope.conjuntos;
					scope.conjunto = STATE.conjuntos[0];
					STATE.conjunto = scope.conjunto;
					departamentos.then(function(data){
						STATE.geoJsonLayer = L.geoJson(data,  {onEachFeature:onEachFeatureNotificaciones}).addTo(map);
						mapCtrl.setActualLayer(STATE.geoJsonLayer);
						init(STATE.geoJsonLayer);
						getInfo().addTo(map);
						$rootScope.ALERTA_STATE = STATE;
						getBroteLegend().addTo(map);
            STATE.info.update();
					});
				});


			} else {
				scope.conjuntos = STATE.conjuntos;
				scope.conjunto = STATE.conjunto;
				STATE.geoJsonLayer.addTo(map);
				if(STATE.inZoom){
					STATE.drillDownLayer.addTo(map);
					map.fitBounds(STATE.drillDownLayer.getBounds());
					STATE.backButton.addTo(map);
					mapCtrl.addControl(STATE.backButton);
					mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
          STATE.info.update();
				}
				STATE.info.addTo(map);
				STATE.broteLegend.addTo(map);
				mapCtrl.addControl(STATE.info);
				mapCtrl.addControl(STATE.broteLegend);
				mapCtrl.setActualLayer(STATE.geoJsonLayer);
			}

			scope.getLabel = function(option){
				return option.anio +' - '+ option.semana +' - '+ option.nombre;
			}

			scope.generarPrediccion = function(){
				if(scope.conjunto!=null){
					setSemPrediccion(scope.conjunto.anio, scope.conjunto.semana);
					STATE.conjunto = scope.conjunto;
					cargarBrotes();
				}

				//Pensar en si se tiene que mostrar semana+1
				//porque para esa semana lo que en realidad es la prediccion
			}

			function init(layer){

	  			setSemPrediccion(scope.conjunto.anio, scope.conjunto.semana);

			    //para conseguir esto llamar al servicio
			    cargarBrotes();

			}

			function cargarBrotes(){
				mapServices.getBroteDepartamentos(STATE.conjunto.id)
	  				.then(function(data){
	    				STATE.departamentos =  data;
	    				return mapServices.getBroteDistritos(STATE.conjunto.id);
				    }).then(function(data){
				    	STATE.distritos = data;
				    	return mapServices.getBroteBarrios(STATE.conjunto.id);
				    })
				    .then(function(data){

						STATE.barrios = data;
						var mapSemDep = new Object();
			    		var mapSemDis = new Object();
			    		var mapSemBar = new Object();

				        for(var i=0; i<STATE.departamentos.length; i++){
				            var obj = STATE.departamentos[i];
				            mapSemDep[obj["departamento"]] = obj;
				        }

				        for(var i=0; i<STATE.distritos.length; i++){
				            var obj = STATE.distritos[i];
				            mapSemDis[obj["distrito"]] = obj;
				        }

				        for(var i=0; i<STATE.barrios.length; i++){
				            var obj = STATE.barrios[i];
				            mapSemDis[obj["barrio"]] = obj;
				        }
				        STATE.casosSemanaBar = mapSemBar;
				        STATE.casosSemanaDis = mapSemDis;
				        STATE.casosSemana = mapSemDep;
				        STATE.geoJsonLayer.setStyle(getStyle);
				        if(STATE.inZoom){
				        	STATE.drillDownLayer.setStyle(getStyleDrillDown);
				        }

	    			})
	    			.catch(function(err){
	    				console.log(err);
	    			});
			}

			function getBroteLegend(){
	  			STATE.broteLegend = L.control({
			        position: 'bottomright'
			    });
			    STATE.broteLegend.onAdd = function(map) {
			    	this._div = L.DomUtil.create('div', 'info legend');
			        this.update();
			        return this._div;
			    };
			    STATE.broteLegend.update = function(map) {
			    	var labels = [];
			    	if(STATE.inZoom){
			    		labels.push('<i style="background:' + getColor('SI') + '"></i> ' + 'Brote SI');
				        labels.push('<i style="background:' + getColor('NO') + '"></i> ' + 'Brote NO');
				        this._div.innerHTML = '<span>Predicción</span><br>' + labels.join('<br>');
			    	} else {
			    		labels.push('<i style="background:' + getColorAlerta(100) + '"></i> ' + '100%');
				        labels.push('<i style="background:' + getColorAlerta(75) + '"></i> ' + '99% - 75%');
				        labels.push('<i style="background:' + getColorAlerta(50) + '"></i> ' + '74% - 50%');
				        labels.push('<i style="background:' + getColorAlerta(25) + '"></i> ' + '49% - 25%');
				        labels.push('<i style="background:' + getColorAlerta(0) + '"></i> ' + '24% - 0%');
				        this._div.innerHTML = '<span>Nivel de alerta</span><br>' + labels.join('<br>');
			    	}
			    };
			    mapCtrl.addControl(STATE.broteLegend);
			    return STATE.broteLegend;
	  		}

			function getInfo(){
				STATE.info = L.control();
			    STATE.info.onAdd = function (map) {
			        this._div = L.DomUtil.create('div', 'info');
			        this.update();
			        return this._div;
			    };
			    STATE.info.update = function (props) {

			        if(props){

			            var dep = props.dpto_desc;
		                var mapSem = STATE.casosSemana;
			            var brote = '0';
			            var cantidad = '-';
			            var total = "-"
			            try{
			                brote = mapSem[dep]["brote"];
			                cantidad = mapSem[dep]["cantidad"];
			                total = mapSem[dep]["total"];
			            }catch(e){

			            }
			            this._div.innerHTML =  '<b>Mapa de Alertas de Brote</b><br/>'
			          					+ 'Predicción para: año <b>'+ STATE.anio
			          					+ '</b>, semana  <b>' + STATE.semana + '</b>';

			            if(props.dpto_desc=='ASUNCION'){
			            	this._div.innerHTML =  this._div.innerHTML + '<br/><b>Dpto:</b> CAPITAL'
			          					+ '<br/><b>' + cantidad + '</b> barrio/s de ' + total+ ' con brote<br/>'
			          					+ '<b>'+brote+'%</b>';
			            } else {
				            this._div.innerHTML =  this._div.innerHTML + '<br/><b>Dpto:</b> ' + dep
				          					+ '<br/><b>' + cantidad + '</b> distrito/s de ' + total+ ' con brote<br/>'
				          					+ '<b>'+brote+'%</b>';
			            }
			          //this._div.innerHTML =  '<h2>Año: '+STATE.anio+' - Semana: '+STATE.semana+'<\/h2><h2>Dpto: '+dep+'<\/h2><h2>Borte: '+brote+'<\/h2>';
			        } else {
                this._div.innerHTML = '<b>Mapa de Alerta de Brote</b><br/>';
              }
			    };
			    STATE.info.updateDrillDown = function (props){

			        if(props){

			            var dep = props.dpto_desc;
			            var depAsu = props.dpto_desc;
			            var dis = props.dist_desc;
			            var mapSem = STATE.casosSemanaDis;
			            var brote = '-';

			            var info;

		                var key = dep + '-' + dis;
		                if(dep == 'ASUNCION'){
		                	key = dep + '-' + props.barlo_desc;
		                }
		                try{
		                    brote = mapSem[key]["brote"];
		                }catch(e){

		                }

		                info = '<b>Mapa de Alertas de Brote</b><br/>'
			                	+ '<b>Año:</b> ' + STATE.anio
			                	+' - <b>Semana:</b> '+ STATE.semana
			                	+ '<br/><b>Dpto:</b> ' + dep
			                	+ '<br/><b>Distrito:</b> ' + dis;
			            if(dep == 'ASUNCION'){
			            	info = info + '<br/><b>Barrio:</b> ' + props.barlo_desc;
			            }
			            info = info + '<br/><b>Brote:</b> ' + brote + '<br/>';
		                //info = '<h2>Año: '+STATE.anio+' - Semana: '+STATE.semana+'<\/h2><h2>Dpto: '+dep+'<\/h2><h2>Distrito: '+dis+'<\/h2><h2>Brote: '+brote+'<\/h2>';


			          this._div.innerHTML =  info;
			        }
		    	};
		    	mapCtrl.addControl(STATE.info);
		    	return STATE.info;
		    }

			function getColor(d) {
			    return  d == 'SI' ? '#FF5454' :
			            d == 'NO' ? '#BFF574' :
			                        '#FFFFFF';
            }

            function getColorAlerta(d) {
			    return  d == 100  ? '#FE0516' :
			            d >= 75   ? '#FD6C24' :
			            d >= 50   ? '#FDFC58' :
			            d >= 25   ? '#BCFD82' :
			            d >= 0    ? '#8EF435' :
			                        '#FFFFFF';
            }

			function getStyle (feature) {
			    var n = feature.properties.dpto_desc;
			    var mapSem = STATE.casosSemana;
			    var color = 'NONE';

			    try{
			        color = mapSem[n]["brote"];
			    }catch(e){
			    }
			 	if(STATE.inZoom){
			    	return { weight: 2,
			            opacity: 1,
			            color: 'white',
			            dashArray: '3',
			            fillOpacity: 0,
			            fillColor: getColor(color)
			        };
			    } else {
			    	return { weight: 2,
				        opacity: 0.9,
				        color: 'white',
				        dashArray: '3',
				        fillOpacity: 0.8,
				        fillColor: getColorAlerta(color)
			    	};
			    }

			}

			function onEachFeatureNotificaciones(feature, layer) {
			    layer.on({
			        mouseover: mouseoverNotificaciones,
			        mouseout: mouseoutNotificaciones,
			        click: zoomToFeature
			    });
			}
			/*Evento al ubicarse el puntero sobre un distrito*/
			function mouseoverNotificaciones(e) {
			    var layer = e.target;
			     layer.setStyle({
			        weight: 5,
			        color: '#666',
			        dashArray: ''

			    });

			    if (!L.Browser.ie && !L.Browser.opera) {
			        layer.bringToFront();
			    }
			   STATE.info.update(layer.feature.properties);
			}

			/*Evento al salir el puntero de un distrito*/
			function mouseoutNotificaciones(e) {
			   // STATE.geoJsonLayer.resetStyle(e.target);
			    var layer = e.target;
			     layer.setStyle({
			        weight: 1,
			        color: '#FFF',
			        dashArray: ''

			    });
			    STATE.info.update();
			}
			var backClass =  L.Control.extend({
		        options: {
		            position: 'topright'
		        },

		        onAdd: function (map) {
		            var controlDiv = L.DomUtil.create('div', 'leaflet-control-command');
		            L.DomEvent
		                .addListener(controlDiv, 'click', L.DomEvent.stopPropagation)
		                .addListener(controlDiv, 'click', L.DomEvent.preventDefault)
		            .addListener(controlDiv, 'click', drillUp);

		            var controlUI = L.DomUtil.create('div', 'leaflet-control-command-interior btn btn-sm btn-primary', controlDiv);
		            controlUI.title = 'Volver a alerta por dptos';
		            return controlDiv;
		        }
		    });
			function zoomToFeature(e) {
				var target = e.target;

				if(!STATE.inZoom){
				   	STATE.backButton = new backClass();
			        map.addControl(STATE.backButton);
			        mapCtrl.addControl(STATE.backButton);
			    }
				if(target.feature.properties.dpto_desc == 'ASUNCION'){
			    	 mapServices.getBarrios(target.feature.properties.dpto_desc)
			    	.then(function(data){
	    				var json = data;
	    				STATE.inZoom = true;
	    				STATE.geoJsonLayer.setStyle(getStyle);
					    if(STATE.drillDownLayer){
					        map.removeLayer(STATE.drillDownLayer);
					    }
					    STATE.drillDownLayer = L.geoJson(json,  {style: getStyleDrillDown, onEachFeature: onEachFeatureDrillDown}).addTo(map);
					    map.fitBounds(target.getBounds());

					    mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
					   	STATE.broteLegend.update();
	    			});
			    } else {
			    	mapServices.getDistritos(target.feature.properties.dpto_desc)
			    	.then(function(data){
	    				var json = data;
	    				STATE.inZoom = true;
	    				STATE.geoJsonLayer.setStyle(getStyle);
					    if(STATE.drillDownLayer){
					        map.removeLayer(STATE.drillDownLayer);
					    }
					    STATE.drillDownLayer = L.geoJson(json,  {style: getStyleDrillDown, onEachFeature: onEachFeatureDrillDown}).addTo(map);
					    map.fitBounds(target.getBounds());
					    mapCtrl.setDrillDownLayer(STATE.drillDownLayer);
						STATE.broteLegend.update();
	    			});
		    	}

			}

			function onEachFeatureDrillDown(feature, layer) {
			    layer.on({
			        mouseover: mouseoverDrillDown,
			        mouseout: mouseoutDrillDown,
			    });
			}
			/*Evento al ubicarse el puntero sobre un distrito*/
			function mouseoverDrillDown(e) {
			    var layer = e.target;
			     layer.setStyle({
			        weight: 5,
			        color: '#666',
			        dashArray: ''

			    });

			    if (!L.Browser.ie && !L.Browser.opera) {
			        layer.bringToFront();
			    }
			    STATE.info.updateDrillDown(layer.feature.properties);
			}
			/*Evento al salir el puntero de un distrito*/
			function mouseoutDrillDown(e) {
			    STATE.drillDownLayer.resetStyle(e.target);
			    STATE.info.updateDrillDown();

			}

			function getStyleDrillDown(feature) {
			    var prop = feature.properties;
			    var n = prop.dpto_desc+'-'+prop.dist_desc;
			    if(prop.dpto_desc == 'ASUNCION'){
			    	n = prop.dpto_desc+'-'+prop.barlo_desc;
			    }
			    var mapSem = STATE.casosSemanaDis;
			    var color = 'NONE';
			   try{
			        color = mapSem[n]["brote"];

			    }catch(e){
			    }

			    return { weight: 2,
			            opacity: 1,
			            color: 'white',
			            dashArray: '3',
			            fillOpacity: 0.5,
			            fillColor: getColor(color)
			        };
			}
			function drillUp(){
			    //map.fitBounds(SMV.STATE.geoJsonLayer.getBounds());
			    map.removeLayer(STATE.drillDownLayer);
			    STATE.drillDownLayer = undefined;
			   	STATE.inZoom = false;
			    STATE.geoJsonLayer.setStyle(getStyle);
			    STATE.backButton.removeFrom(map);
			    mapCtrl.removeControl(STATE.backButton);
			    map.setView([-23.388, -60.189], 7);
			}

			function setSemPrediccion(anio, semana){
				if(semana == '52' && (anio == '2014' || anio == '2009' || anio == '2015')){
					STATE.anio = anio;
					STATE.semana = '53'
				} else if(semana == '52' || semana == '53'){
					STATE.semana = '1';
					var anio2 = parseInt(anio)+1;
					STATE.anio = anio2+'';
				} else {
					var semana2 = parseInt(semana)+1;
					STATE.semana = semana2+'';
					STATE.anio = anio;
				}
			}

			scope.refreshLista = function(anio){
				console.log('refreshLista');
				if(anio=='-'){
					mapServices.getListaConjuntos()
					.then(function(data){
						scope.conjuntos = data;
						STATE.conjuntos = data;
						if(data.length > 0){
							scope.conjunto = data[0];
						} else {
							scope.conjunto = null;
						}
					});
				} else {
					mapServices.getListaConjuntosAnio(anio)
					.then(function(data){
						scope.conjuntos = data;
						STATE.conjuntos = data;
						if(data.length > 0){
							scope.conjunto = data[0];
						} else {
							scope.conjunto = null;
						}
					});
				}

			}
    	}
    };
});
