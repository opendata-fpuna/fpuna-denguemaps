'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:AyudaCtrl
 * @description
 * # AyudaCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('AyudaCtrl', function ($scope, $location) {
    $scope.steps;

    $scope.clickHelp = function () {
      console.log("clickHelp");
      //$('#mapa').hasClass('active');
      if ($location.path() == '/') {
        $scope.steps = $scope.stepsMapa;
      } else if ($location.path() == '/listado') {
        $scope.steps = $scope.stepsListado;
      } else {
        $scope.steps = false;
      }

      introJs().setOptions({
        doneLabel: 'Salir',
        nextLabel: 'Siguiente &rarr;',
        prevLabel: '&larr; Anterior',
        skipLabel: 'Salir',
        steps: $scope.steps
      }).start();
    }

    $scope.stepsMapa = [
    {
      intro: "Bienvenido a esta visualización interactiva.</br></br>Este tutorial te guiará paso a paso a través de las diferentes funcionalidades disponibles. \
      </br></br>Haz click en siguiente para comenzar."
    },
    {
      element: '#tab-mapa',
      intro: "En esta sección, puedes ver los diferentes mapas de Dengue en el Paraguay.",
      position: "right"
    },
    {
      element: '.sidebar-tabs',
      intro: "Puedes seleccionar el tipo de mapa y realizar filtros sobre los resultados.",
      position: 'right'
    },
    {
      element: '.leaflet-control-zoom',
      intro: "Acerca y aleja el mapa con este componente.",
      position: "left"
    },
    {
      element: '.leaflet-control-layers',
      intro: "Cambia el mapa base con este componente.</br></br>¿Te gustaría ver una imagen satelital? Es posible!",
      position: "left"
    },
    /*{
      element: '.opacity_slider_control',
      intro: "Cambia la opacidad de las capas superpuestas con este componente.",
      position: "left"
    },*/
    {
      element: '#tab-listado',
      intro: "En la sección de listado, puedes ver datos de las notificaciones de dengue de forma tabular. Visítala!",
      position: "right"
    },
    {
      element: '#tab-diccionario',
      intro: "En la sección de diccionario, puedes ver el diccionario de datos de forma tabular. Visítala!",
      position: "right"
    },
    {
      element: '#tab-datos',
      intro: "En la sección de datos, puedes cargar nuevos datos. Visítala!",
      position: "right"
    },
    {
      element: '#tab-descargas',
      intro: "Haciendo click aquí puedes descargar los datos por año en formato CSV y JSON.",
      position: "right"
    },
    /*{
      element: '.info',
      intro: "Aquí puedes ver los datos del departamento/distrito/barrio visible en el mapa.",
      position: "left"
    },*/
    {
      element: '#tab-alertas',
      intro: "En la sección de alertas, genera alertas individuales y múltiples de brote de dengue en Paraguay.",
      position: "right"
    }
  ];

  $scope.stepsListado = [
    {
      intro: "Bienvenido a esta visualización interactiva.</br></br>Este tutorial te guiará paso a paso a través de las diferentes funcionalidades disponibles. \
      </br></br>Haz click en siguiente para comenzar."
    },
    {
      element: '#tab-listado',
      intro: "En esta sección, puedes ver datos de las notificaciones de dengue de forma tabular.",
      position: "right"
    },
    {
      element: '.dataTables_length > label',
      intro: "Selecciona la cantidad de filas por página de la tabla.",
      position: 'right'
    },
    {
      element: '.dataTables_filter > label',
      intro: "Filtra los resultados de acuerdo a los valores de cualquier campo.",
      position: "left"
    },
    {
      element: 'tfoot',
      intro: "O filtra de acuerdo a los valores de una columna en particular.",
      position: "bottom"
    },
    {
      element: '.dataTables_info',
      intro: "Aquí puedes ver un resumen de los resultados de tu búsqueda.",
      position: "right"
    },
    {
      element: '.dataTables_paginate',
      intro: "Si los resultados son muchos, desplázate entre las páginas de la tabla.",
      position: "left"
    },
    {
      element: '#download-footer',
      intro: "Descarga los resultados filtrados en JSON o CSV.",
      position: "top"
    },
    {
      element: '#tab-diccionario',
      intro: "En la sección de diccionario, puedes ver el diccionario de datos de forma tabular. Visítala!",
      position: "right"
    },
    {
      element: '#tab-datos',
      intro: "En la sección de datos, puedes cargar nuevos datos. Visítala!",
      position: "right"
    },
    {
      element: '#tab-descargas',
      intro: "Haciendo click aquí puedes descargar los datos por año en formato CSV y JSON.",
      position: "right"
    },
    {
      element: '#tab-alertas',
      intro: "En la sección de alertas, genera alertas individuales y múltiples de brote de dengue en Paraguay.",
      position: "right"
    },
    {
      element: '#tab-mapa',
      intro: "Por último, ¿ya visitaste la sección del mapa interactivo?",
      position: "right"
    }
  ];

});
