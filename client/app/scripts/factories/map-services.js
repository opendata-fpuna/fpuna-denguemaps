'use strict';



angular.module('denguemapsApp')
  .factory('mapServices', function($http, $q) {
    var dengueURL = window.basePath + '/denguemaps-server/rest';
    var geoURL = window.basePathGeo + '/geoserver/sf/ows';
  	return {
  		getRiesgosDepartamentos: getRiesgosDepartamentos,
      getRiesgosDistritos: getRiesgosDistritos,
      getRiesgosAsuncion: getRiesgosAsuncion,
  		getDepartamentos: getDepartamentos,
      getDistritos: getDistritos,
      getBarrios: getBarrios,
      getCasosDepartamentos: getCasosDepartamentos,
      getIncidenciaDepartamentos: getIncidenciaDepartamentos,
      getIncidenciaDistritos: getIncidenciaDistritos,
      getIncidenciaAsuncion: getIncidenciaAsuncion,
      getPoblacionPorDepto: getPoblacionPorDepto,
      getPoblacionPorDistrito: getPoblacionPorDistrito,
      getPoblacionPorBarrioAsu: getPoblacionPorBarrioAsu,
      esBrote: esBrote,
      getDepartamentosPrincipales: getDepartamentosPrincipales,
      getListaConjuntos: getListaConjuntos,
      getBroteDepartamentos: getBroteDepartamentos,
      getBroteDistritos: getBroteDistritos,
      getListaConjuntosAnio: getListaConjuntosAnio,
      getBroteBarrios: getBroteBarrios
  	};
    
  	function getRiesgosDepartamentos(anio){
  		var defered = $q.defer();
  		var promise = defered.promise;
      var url = dengueURL + '/notificacion/riesgo/' + anio;
  		$http.get(url, {cache: true})
  			.success(function(data){
  				defered.resolve(data);
  			})
  			.error(function(err){
  				defered.reject(err);
  			});
  		return promise;
  	}

    function getRiesgosDistritos(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/riesgo/distrito/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getRiesgosAsuncion(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/riesgo/asuncion/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    /* EN LA OFICINA AGREGARLE EL NRO 2 */
  	function getDepartamentos(){
  		var defered = $q.defer();
  		var promise = defered.promise;
		  //var url = 'http://192.168.1.106:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_departamento_paraguay&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      var url = geoURL + '?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_departamento_paraguay_simply&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
  		$http.jsonp(url, {cache: true})
  			.success(function(data){
  				defered.resolve(data);
  			})
  			.error(function(err){
  				defered.reject(err);
  			});
  		return promise;
  	}

    /* ESTOS DOS TDV NO SE ESTAN USANDO, FALTA PROBAR */
    function getDistritos(dpto){
      var defered = $q.defer();
      var promise = defered.promise;
      //var url = 'http://192.168.1.106:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_distrito_paraguay&CQL_FILTER=first_dp_1=%27' + dpto + '%27&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      var url = geoURL + '?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_distrito_paraguay&CQL_FILTER=dpto_desc=%27' + dpto + '%27&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      $http.jsonp(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getBarrios(dpto){
      var defered = $q.defer();
      var promise = defered.promise;
      //var url = 'http://192.168.1.106:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_barrio_localidad_paraguay&CQL_FILTER=dpto_desc=%27' + dpto + '%27&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      var url = geoURL + '?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_barrio_localidad_paraguay&CQL_FILTER=dpto_desc=%27' + dpto + '%27&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      $http.jsonp(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getCasosDepartamentos(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/filtrosmapa?anio=' + anio +'&confirmado='+1+'&descartado='+1+'&sospechoso='+1+'&f=0&m=0';
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getIncidenciaDepartamentos(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/incidencia/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getIncidenciaDistritos(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/incidencia/distrito/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getIncidenciaAsuncion(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/incidencia/asuncion/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }


    function getPoblacionPorDepto(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/poblacion/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getPoblacionPorDistrito(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/poblacion/distrito/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getPoblacionPorBarrioAsu(){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/notificacion/poblacion/asuncion/';
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function esBrote(registro){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/prediccion/individual';
      url = url + '?cantidad_total='+registro.cant_0
        +'&incidencia='+registro.incidencia
        +'&cantidadanterior='+registro.cant_1
        +'&cantanteanterior='+registro.cant_2
        +'&anio='+registro.anio
        +'&semana='+registro.semana
        +'&departamento='+registro.dpto.id
        +'&distrito='+registro.dist.id
        +'&brote='+registro.brote;
        if(registro.barrio!=null){
           url = url + '&barrio='+registro.barrio.id;
        } else {
          url = url + '&barrio=null';
        }
      $http.post(url,registro)
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    //crear tabla en la base de datos postgis
    //create table mapa_departamentos_principales as ( select * from mapa_departamento_paraguay_simply where dpto in ('10','00','11','07'))
    //y agregar la capa en el geoserver
    function getDepartamentosPrincipales(){
      var defered = $q.defer();
      var promise = defered.promise;
      //var url = 'http://192.168.1.106:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_departamento_paraguay&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      var url = geoURL + '?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_departamentos_principales&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
      $http.jsonp(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getListaConjuntos(){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/prediccion/conjuntos';
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getListaConjuntosAnio(anio){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/prediccion/conjuntos/' + anio;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getBroteDepartamentos(conjuntoId){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/prediccion/departamentos/'+ conjuntoId;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getBroteDistritos(conjuntoId){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/prediccion/distritos/' + conjuntoId;
      $http.get(url, {cache: true})
        .success(function(data){
            defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }

    function getBroteBarrios(conjuntoId){
      var defered = $q.defer();
      var promise = defered.promise;
      var url = dengueURL + '/prediccion/barrios/' + conjuntoId;
      $http.get(url, {cache: true})
        .success(function(data){
          defered.resolve(data);
        })
        .error(function(err){
          defered.reject(err);
        });
      return promise;
    }
});